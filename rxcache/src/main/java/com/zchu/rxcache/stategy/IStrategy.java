package com.zchu.rxcache.stategy;

/**
 * IStrategy interface
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public interface IStrategy extends IObservableStrategy,IFlowableStrategy {

}
