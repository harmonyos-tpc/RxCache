package com.zchu.rxcache.stategy;


import com.zchu.rxcache.RxCache;
import com.zchu.rxcache.data.CacheResult;
import io.reactivex.rxjava3.core.Observable;

import java.lang.reflect.Type;

/**
 * IObservableStrategy interface
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public interface IObservableStrategy {

    <T> Observable<CacheResult<T>> execute(RxCache rxCache, String key, Observable<T> source, Type type);
}
