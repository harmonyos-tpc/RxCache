package com.zchu.rxcache.stategy;

/**
 * CacheStrategy class to pass the strategy from client app
 *
 * @author 赵成柱
 * @since 2021-02-09
 */

public final class CacheStrategy {

    /**
     * Preferred network, cache stored in asynchronous mode
     *
     * @return IStrategy
     */
    public static IStrategy firstRemote() {

        return new FirstRemoteStrategy();
    }

    /**
     * Preferred network, and cached data is stored in synchronous mode.
     *
     * @return IStrategy
     */
    public static IStrategy firstRemoteSync() {
        return new FirstRemoteStrategy(true);
    }

    /**
     * Preferential cache. Cache is stored in asynchronous mode.
     *
     * @return IStrategy
     */
    public static IStrategy firstCache() {
        return new FirstCacheStrategy();
    }

    /**
     * 优先缓存,缓存用同步的方式保存
     *
     * @return IStrategy
     */
    public static IStrategy firstCacheSync() {
        return new FirstCacheStrategy(true);
    }

    /**
     * Preferentially cache data and set the timeout interval.
     *
     * @param milliSecond millisecond
     * @return IStrategy
     */
    public static IStrategy firstCacheTimeout(long milliSecond) {
        return new FirstCacheTimeoutStrategy(milliSecond);
    }

    /**
     * The cache is preferentially cached and the timeout interval is set. The cache is stored in synchronous mode.
     *
     * @param milliSecond
     * @return IStrategy
     */
    public static IStrategy firstCacheTimeoutSync(long milliSecond) {
        return new FirstCacheTimeoutStrategy(milliSecond, true);
    }


    /**
     * Only the network is loaded, but the data is still cached.
     *
     * @return IStrategy
     */
    public static IStrategy onlyRemote() {
        return new OnlyRemoteStrategy();
    }

    /**
     * Only the network is loaded, but the data is still cached.
     *
     * @return IStrategy
     */
    public static IStrategy onlyRemoteSync() {
        return new OnlyRemoteStrategy(true);
    }

    /**
     * Load Cache Only
     *
     * @return IStrategy
     */
    public static IStrategy onlyCache() {
        return new OnlyCacheStrategy();
    }

    /**
     * The cache is loaded before the network. The cache is stored in asynchronous mode.
     *
     * @return IStrategy
     */
    public static IStrategy cacheAndRemote() {
        return new CacheAndRemoteStrategy();
    }

    /**
     * he cache is loaded and then the network is loaded. The cache is stored in synchronous mode.
     *
     * @return IStrategy
     */
    public static IStrategy cacheAndRemoteSync() {
        return new CacheAndRemoteStrategy(true);
    }

    /**
     * Loads only the network, not caches.
     *
     * @return IStrategy
     */
    public static IStrategy none() {
        return NoneStrategy.INSTANCE;
    }
}
