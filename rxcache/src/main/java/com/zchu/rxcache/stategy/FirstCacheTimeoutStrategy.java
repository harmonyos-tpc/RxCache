package com.zchu.rxcache.stategy;

import com.zchu.rxcache.CacheTarget;
import com.zchu.rxcache.RxCache;
import com.zchu.rxcache.RxCacheHelper;
import com.zchu.rxcache.data.CacheResult;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Predicate;
import org.reactivestreams.Publisher;

import java.lang.reflect.Type;


/**
 * Preferential cache and configurable timeout interval
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public final class FirstCacheTimeoutStrategy implements IStrategy {
    private boolean isSync;
    private long milliSecond;

    /**
     * @param milliSecond milliseconds
     */
    public FirstCacheTimeoutStrategy(long milliSecond) {
        this(milliSecond, false);
    }

    /**
     * @param milliSecond milliseconds
     * @param isSync      Indicates whether to save the cache in synchronous mode.
     */
    public FirstCacheTimeoutStrategy(long milliSecond, boolean isSync) {
        this.isSync = isSync;
        this.milliSecond = milliSecond;
    }

    @Override
    public <T> Observable<CacheResult<T>> execute(RxCache rxCache, String key, Observable<T> source, Type type) {
        Observable<CacheResult<T>> cache = RxCacheHelper.loadCache(rxCache, key, type, true);
        cache = cache.filter(new Predicate<CacheResult<T>>() {
            @Override
            public boolean test(CacheResult<T> tCacheResult) throws Exception {
                return System.currentTimeMillis() - tCacheResult.getTimestamp() <= milliSecond;
            }
        });
        Observable<CacheResult<T>> remote;
        if (isSync) {
            remote = RxCacheHelper.loadRemoteSync(rxCache, key, source, CacheTarget.MemoryAndDisk, false);
        } else {
            remote = RxCacheHelper.loadRemote(rxCache, key, source, CacheTarget.MemoryAndDisk, false);
        }
        return cache.switchIfEmpty(remote);
    }

    @Override
    public <T> Publisher<CacheResult<T>> flow(RxCache rxCache, String key, Flowable<T> source, Type type) {
        Flowable<CacheResult<T>> cache = RxCacheHelper.loadCacheFlowable(rxCache, key, type, true);
        cache = cache.filter(new Predicate<CacheResult<T>>() {
            @Override
            public boolean test(CacheResult<T> tCacheResult) throws Exception {
                return System.currentTimeMillis() - tCacheResult.getTimestamp() <= milliSecond;
            }
        });
        Flowable<CacheResult<T>> remote;
        if (isSync) {
            remote = RxCacheHelper.loadRemoteSyncFlowable(rxCache, key, source, CacheTarget.MemoryAndDisk, false);
        } else {
            remote = RxCacheHelper.loadRemoteFlowable(rxCache, key, source, CacheTarget.MemoryAndDisk, false);
        }
        return cache.switchIfEmpty(remote);
    }
}
