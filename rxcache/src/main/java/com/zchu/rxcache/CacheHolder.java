package com.zchu.rxcache;

/**
 * CacheHolder
 *
 * @author 赵成柱
 * @since 2021-02-09
 * @param <T>
 */
public class CacheHolder<T> {

    public CacheHolder(T data, long timestamp) {
        this.data = data;
        this.timestamp = timestamp;
    }

    public T data;
    public long timestamp;
}
