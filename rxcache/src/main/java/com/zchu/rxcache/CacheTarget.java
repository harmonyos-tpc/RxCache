package com.zchu.rxcache;

/**
 * Cache Target
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public enum CacheTarget {
    Memory,
    Disk,
    MemoryAndDisk;

    public boolean supportMemory() {
        return this==Memory || this== MemoryAndDisk;
    }

    public boolean supportDisk() {
        return this==Disk || this== MemoryAndDisk;
    }

}
