package com.zchu.rxcache.diskconverter;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

/**
 * Universal converter
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public interface IDiskConverter {

    /**
     * Fetch
     *
     * @param source
     * @param type
     * @return T
     */
    <T> T load(InputStream source, Type type);

    /**
     * Write
     *
     * @param sink
     * @param data
     * @return boolean
     */
    boolean writer(OutputStream sink, Object data);

}
