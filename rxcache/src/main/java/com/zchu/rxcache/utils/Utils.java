package com.zchu.rxcache.utils;

import ohos.media.image.PixelMap;

import java.io.Closeable;
import java.io.IOException;


/**
 * utility class 
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public final class Utils {

    private Utils() {
    }

    public static void close(Closeable close) {
        if (close != null) {
            try {
                closeThrowException(close);
            } catch (IOException ignored) {
            }
        }
    }

    public static void closeThrowException(Closeable close) throws IOException {
        if (close != null) {
            close.close();
        }
    }

    public static String getFormat(PixelMap bitmap) {

        if (ohos.media.image.common.AlphaType.PREMUL == bitmap.getImageInfo().alphaType) {
            return "image/png";
        } else {
            return "image/jpeg";
        }
    }
}
