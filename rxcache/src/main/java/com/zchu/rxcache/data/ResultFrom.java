package com.zchu.rxcache.data;

/**
 * Data Source
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public enum ResultFrom {
    Remote, Disk, Memory;

    public static boolean ifFromCache(ResultFrom from) {
        return from == Disk || from == Memory;
    }
}
