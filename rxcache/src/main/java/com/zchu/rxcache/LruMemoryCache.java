package com.zchu.rxcache;


import com.zchu.rxcache.utils.LogUtils;
import com.zchu.rxcache.utils.MemorySizeOf;
import com.zchu.rxcache.utils.Occupy;

import ohos.media.image.PixelMap;

import java.util.HashMap;


/**
 * LruMemoryCache
 *
 * @author 赵成柱
 * @since 2021-02-09
 */
public class LruMemoryCache {
    private LruCache<String, Object> mCache;
    private HashMap<String, Integer> memorySizeMap;//Stores the size of the object that is added to the cache for the first time to avoid measurement
    // errors caused by the change of the object size in the memory.
    private HashMap<String, Long> timestampMap;
    private Occupy occupy;

    public LruMemoryCache(final int cacheSize) {
        memorySizeMap = new HashMap<>();
        timestampMap = new HashMap<>();
        byte to = 0;
        byte t4 = 4;
        occupy = new Occupy(to, to, t4);
        mCache = new LruCache<String, Object>(cacheSize) {
            @Override
            protected int sizeOf(String key, Object value) {
                Integer integer = memorySizeMap.get(key);
                if (integer == null) {
                    integer = countSize(value);
                    memorySizeMap.put(key, integer);
                }
                return integer;
            }

            @Override
            protected void entryRemoved(boolean evicted, String key, Object oldValue, Object newValue) {
                super.entryRemoved(evicted, key, oldValue, newValue);
                memorySizeMap.remove(key);
                timestampMap.remove(key);
            }
        };
    }

    public <T> CacheHolder<T> load(String key) {
        T value = (T) mCache.get(key);
        if (value != null) {
            return new CacheHolder<>(value, timestampMap.get(key));
        }
        return null;
    }

    public <T> boolean save(String key, T value) {
        if (null != value) {

            mCache.put(key, value);
            timestampMap.put(key, System.currentTimeMillis());
        }
        return true;
    }

    public boolean containsKey(String key) {
        return mCache.get(key) != null;
    }

    /**
     * Deleting a Cache
     *
     * @param key
     * @return boolean
     */
    public boolean remove(String key) {
        Object remove = mCache.remove(key);
        if (remove != null) {
            memorySizeMap.remove(key);
            timestampMap.remove(key);
            return true;
        }
        return false;
    }

    public void clear() {
        mCache.evictAll();
    }

    protected int countSize(Object value) {
        if (value == null) {
            return 0;
        }

        //  Better memory size algorithm
        int size;
        if (value instanceof PixelMap) {
            LogUtils.debug("PixelMap");
            size = MemorySizeOf.sizeOf((PixelMap) value);
        } else {
            size = occupy.occupyof(value);
        }
        LogUtils.debug("size=" + size + " value=" + value);
        return size > 0 ? size : 1;
    }

}
