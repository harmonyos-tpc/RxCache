# RxCache

## Introduction:
This is a framework dedicated to RxJava, which solves the problem of caching the results issued by any Observable in openharmony.


### Features
#### Cache level support

* Observable
* LruCache
* DiskLruCache

#### Existing storage strategies

* Priority network
* Priority cache
* Prioritize caching and set a timeout
* Only load the network, but the data will still be cached
* Load the cache first, then load the network
* Only load the network, not cache


## Usage Instruction:

```java
rxCache = new RxCache.Builder().appVersion(2)
                      .diskDir(new File(getCacheDir().getPath() + File.separator + "data-cache"))
                      .diskConverter(new GsonDiskConverter())
                      .diskMax(20 * 1024 * 1024)
                      .memoryMax(2 * 1024 * 1024)
                      .setDebug(true)
                      .build();
```
Reuse compose() operator transformation
```java
serverAPI.fetchUsers()
    .compose(rxCache.transformObservable("custom_key", new TypeToken<String>() { }.getType(), strategy))
    .subscribeOn(Schedulers.io())
    .observeOn(OpenHarmonySchedulers.mainThread())
    .subscribe(new Observer<CacheResult<String>>() {
        @Override
        public void onSubscribe(@NonNull Disposable disposable) {

            mSubscription = disposable;
        }
		...
	}
	
```
### Retrofit

On the basis of the original code if you are using retrofit , you only need 2 lines of code to get it done in one step.

#### Observable
```java
//注意在 <~> 中声明数据源的类型
.compose(rxCache.<~>transformObservable（key,type,CacheStrategy.firstCache()))
.map(new CacheResult.MapFunc<~>())
```
#### Flowable
```java
.compose(rxCache.<~>transformFlowable（key,type,CacheStrategy.firstCache()))
.map(new CacheResult.MapFunc<~>())
```
Just declare the caching strategy here, without affecting the original code structure

If you are entangled in the name of the key value, it is recommended to use ("method name" + "parameter name: "+"add parameter value").

### CacheStrategy
#### CacheStrategyProvide the following caching strategy in the class:

 Strategic choice                   | Summary
 ------------------------- | ------- 
 firstRemote()             | Priority network
 firstCache() |Priority cache
 firstCacheTimeout(milliSecond) |Prioritize caching and set a timeout
 onlyRemote() | Only load the network, but the data will still be cached
 onlyCache()           | Load cache only
 cacheAndRemote()              | Load the cache first, then load the network
 none()              | Only load the network, not cache

If you need to save in a synchronous manner, each strategy has a corresponding synchronous save method. For example, if you CacheStrategy.firstRemoteSync() use the synchronous save method, the data will not respond until the cache is written.

### CacheResult
#### CacheResult The class contains the following attributes:

```java
public class CacheResult<T> {
    private ResultFrom from;//数据来源，原始observable、内存或硬盘
    private String key;
    private T data; // 数据
    private long timestamp; //数据写入到缓存时的时间戳，如果来自原始observable则为0
	...
}
```


### Default rxCache
You can also use the default RxCache:
Initialize the defaultRxCache
```java
RxCache.initializeDefault(rxcache)
```
Use again like this:
```java
observable
	.compose(RxCache.getDefault().<~>>transformObservable("custom_key", type, strategy))
	...
```
If you do not initialize default RxCache, this cache is saved to use Environment.getDownloadCacheDirectory()
and appVersion will be forever 1.




### Generic
Because of the erasure of generics, when encountering generics like List<~>, you can use it like this:

```java
// <~> is the data type of the List element.
.compose(rxCache.<List<~>>transformer("custom_key", new TypeToken<List<~>>() {}.getType(), strategy))
```

When there is no generic type, Type can be passed directly to Class
```java
.compose(rxCache.<Bean>transformer("custom_key",Bean.class, strategy))
```


### Basic usage

#### Save the cache:
Such as saving the string to the memory and hard disk:
```java
rxCache
	.save("test_key1","RxCache is simple", CacheTarget.MemoryAndDisk)
	.subscribeOn(Schedulers.io())
	.subscribe();
```
There are 3 options for saving:
```java
public enum CacheTarget {
    Memory,
    Disk,
    MemoryAndDisk;
...
}
```


#### Read cache:
The order of reading will follow the order of memory --> hard disk. For example, read the string in the cache:
```java
 rxCache
	.<String>load("test_key1", String.class)
	.map(new CacheResult.MapFunc<String>())
	.subscribe(new Consumer<String>() {
		@Override
		public void accept(String value) throws Exception {
			
		}
	});
```

Get the cache synchronously:
```java
 CacheResult<String> = rxCache.<String>loadSync("test_key1", String.class);
```



### Obfuscated configuration
This Library does not need to add additional obfuscation configuration, so the code can be obfuscated

### Installation tutorial:
#### Method-1:
   Add following har in entry libs folder:
```
    rxcache.har
```   

Add the following code to the gradle of the entry:
```
    implementation fileTree(dir: 'libs', include: ['.jar', '.har'])
```

#### Method-2:
   Add the following code to the gradle of the entry
``` 
    implementation project(path: ':rxcache')
``` 
	
#### Method-3: 
   For using RxCache library from a remote repository in separate application, add the below dependency in entry/build.gradle file: 
```     
    implementation 'io.openharmony.tpc.thirdlib:RxCache:1.0.0'		
```                 